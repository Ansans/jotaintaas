function addItem (event) {
  event.preventDefault()

  const item = event.target.name.value
  document.getElementById("name").value = ""
  const messageArea = event.target.text.value
  document.getElementById("text").value = ""

  if (item === "" || messageArea === "") {
    alert("Your name or message is empty.")
}
  else {

  const liElement = document.createElement("li")
  liElement.classList.add("margins")

  const text = document.createTextNode(item)
  liElement.appendChild(text)
  const kirjoitti = document.createTextNode(" kirjoitti: ")
  liElement.appendChild(kirjoitti)
  const br = document.createElement("br")
  liElement.appendChild(br)
  const message = document.createTextNode(messageArea)
  liElement.appendChild(message)
   const br2 = document.createElement("br")
  liElement.appendChild(br2)
 

  const button = document.createElement('button')
  button.addEventListener('click', () => { 
    liElement.remove();;
  })

  button.textContent = 'delete'
  button.classList.add("nappi2")

  liElement.appendChild(button)
  
  liElement.classList.add("border", "indent");

  const list = document.getElementById("forum")
  list.appendChild(liElement)
}}


const toggle = (event) => {
    event.preventDefault();
    let x = document.getElementById("lomake");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
} 



